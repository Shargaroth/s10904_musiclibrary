package webservices;


import javax.ejb.Stateless;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Album;
import database.UnitOfWork;
import database.builder.IEntityBuilder;
import database.managers.IAlbumManager;
import dto.AlbumDto;
import dto.AlbumSummaryDto;

@Path("album")
@Stateless
public class AlbumRestService {
	
	@Inject
	private IAlbumManager mgr;
	
	@GET
	@Path("/givealbum/{id}")
    @Produces("application/json")
	public AlbumDto giveAlbum(@PathParam("id") int id)
	{
		//UnitOfWork uow = new UnitOfWork();
		//IEntityBuilder<Album> builder = new AlbumBuilder();
		//IAlbumManager mgr = new MysqlAlbumManager(uow,builder,null);
		Album b = mgr.get(id);
		AlbumDto result = new AlbumDto();
		result.setLabel(b.getLabel());
		result.setTitle(b.getTitle());
		result.setYear(b.getYear());
		
		return result;
		
	}
	
	public AlbumSummaryDto saveAlbum(AlbumDto request)
	{
		//UnitOfWork uow = new UnitOfWork();
		//IEntityBuilder<Album> builder = new AlbumBuilder();
		//IAlbumManager mgr = new MysqlAlbumManager(uow,builder,null);
		Album b = new Album();
		b.setLabel(request.getLabel());
		b.setTitle(request.getTitle());
		b.setYear(request.getYear());
		mgr.save(b);
		//uow.commit();
		mgr.saveChanges();
		AlbumSummaryDto result = new AlbumSummaryDto();
		result.setLabel(b.getLabel());
		result.setTitle(b.getTitle());
		result.setYear(b.getYear());
		
		return result;
		
	}
}
