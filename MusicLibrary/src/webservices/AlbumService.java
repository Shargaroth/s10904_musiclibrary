package webservices;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Album;
import database.UnitOfWork;
import database.builder.IEntityBuilder;
import database.managers.IAlbumManager;
import dto.*;

@WebService
public class AlbumService {

	@Inject
	private IAlbumManager mgr;
	
	@WebMethod
	public AlbumDto giveAlbum(int request)
	{
		Album b = mgr.get(request);
		AlbumDto result = new AlbumDto();
		result.setLabel(b.getLabel());
		result.setTitle(b.getTitle());
		result.setYear(b.getYear());
		
		return result;
		
	}
	
	@WebMethod
	public AlbumSummaryDto saveAlbum(AlbumDto request)
	{
		//UnitOfWork uow = new UnitOfWork();
		//IEntityBuilder<Album> builder = new AlbumBuilder();
		//IAlbumManager mgr = new MysqlAlbumManager(uow,builder,null);
		Album b = new Album();
		b.setLabel(request.getLabel());
		b.setTitle(request.getTitle());
		b.setYear(request.getYear());
		mgr.save(b);
		mgr.saveChanges();
		AlbumSummaryDto result = new AlbumSummaryDto();
		result.setLabel(b.getLabel());
		result.setTitle(b.getTitle());
		result.setYear(b.getYear());
		
		return result;
		
	}
}
