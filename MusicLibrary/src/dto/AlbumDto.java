package dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class AlbumDto extends AlbumSummaryDto{

	private List<BandSummaryDto> bands;

	public List<BandSummaryDto> getBands() {
		return bands;
	}

	public void setBands(List<BandSummaryDto> bands) {
		this.bands = bands;
	}
	
	
}
