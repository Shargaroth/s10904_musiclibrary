package database;

public class PageInfo {

	private int pageIndex;
	private int pageSize;
	private int resultCount;
	
	public PageInfo(){}
	
	public PageInfo(int pageIndex, int pageSize, int resultCount) {
		super();
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
		this.resultCount = resultCount;
	}
	
	
	public int getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getResultCount() {
		return resultCount;
	}
	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}
	
	
}
