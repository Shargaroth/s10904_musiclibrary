package database;

import java.util.List;

import database.unitofwork.IUnitOfWork;
import database.unitofwork.IUnitOfWorkRepository;
import model.Album;
import model.EntityBase;

public abstract class ManagerBase<E extends EntityBase> implements IManager<E>,IUnitOfWorkRepository {

	
	protected IUnitOfWork uow;
	

	public ManagerBase(IUnitOfWork uow) {
		super();
		this.uow = uow;
	}

	public void save(E obj) {
		uow.registerAdd(obj, this);
		
	}

	public void delete(E obj) {
		uow.registerDeleted(obj, this);
		
	}
	
	public void update(E obj)
	{
		uow.registerUpdated(obj, this);
	}
	
	public final void saveChanges()
	{
		uow.commit();
	}
	
	public abstract E get(int id);

	public abstract List<E> getAll(PageInfo request);
	
	public abstract void persistAdd(EntityBase ent);

	public abstract void persistDeleted(EntityBase ent);

	public abstract void persistUpdated(EntityBase ent);
	
}
