package database.builder;

import java.sql.ResultSet;

import javax.enterprise.context.RequestScoped;

import model.Band;
@RequestScoped
public class BandBuilder implements IEntityBuilder<Band>{

	public Band build(ResultSet rs) {
		Band a = null;
		try
		{
			a = new Band();
			a.setId(rs.getInt("id"));
			a.setName(rs.getString("name"));
			a.setGenre(rs.getString("genre"));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return a;
	}

}
