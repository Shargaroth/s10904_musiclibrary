package database.builder;

import java.sql.ResultSet;

import javax.enterprise.context.RequestScoped;

import model.Album;
@RequestScoped
public class AlbumBuilder implements IEntityBuilder<Album>{

	public Album build(ResultSet rs) {
		Album result = null;
		try{
			
			result = new Album();
			result.setId(rs.getInt("id"));
			result.setLabel(rs.getString("publisher"));
			result.setTitle(rs.getString("title"));
			result.setYear(rs.getInt("year"));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return result;
	}

}
