package database.managers.impl;

import java.util.List;

import model.Album;
import model.EntityBase;
import database.ManagerBase;
import database.MockDb;
import database.PageInfo;
import database.managers.IAlbumManager;
import database.unitofwork.IUnitOfWork;

public class MockAlbumManager 
	extends ManagerBase<Album> 
	implements IAlbumManager {

	private MockDb db;
	
	public MockAlbumManager(IUnitOfWork uow, MockDb db)
	{
		this(uow);
		this.db=db;
	}
	
	private MockAlbumManager(IUnitOfWork uow) {
		super(uow);
	}

	public void setBands(Album b) {
	
	}

	@Override
	public Album get(int id) {
		Album album = new Album();
		Album b = (Album)db.get(id-1);
		album.setId(b.getId());
		album.setLabel(b.getLabel());
		album.setTitle(b.getTitle());
		album.setYear(b.getYear());
		return album;
	}

	@Override
	public List<Album> getAll(PageInfo request) {
		// TODO Auto-generated method stub
		return db.getItemsByType(Album.class);
	}

	@Override
	public void persistAdd(EntityBase ent) {
		db.save(ent);
		
	}

	@Override
	public void persistDeleted(EntityBase ent) {
		db.delete(ent);
		
	}

	@Override
	public void persistUpdated(EntityBase ent) {
		//  tu nie trzeba nic implementować póki co 
		
	}

}
