package database.managers;

import model.Enumeration;
import database.IManager;

public interface IEnumerationManager extends IManager<Enumeration>{

	public Object getEnumKey(int key,String stringKey); 
}
