package database.managers.album.tests;

import static org.junit.Assert.*;
import model.Album;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import database.MockDb;
import database.UnitOfWork;
import database.managers.IAlbumManager;
import database.managers.impl.MockAlbumManager;
import database.unitofwork.IUnitOfWork;

public class AlbumManagerTests {

	private  MockDb db;
	private IUnitOfWork uow;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		db = new MockDb();
		Album b1 = new Album();
		Album b2 = new Album();
		Album b3 = new Album();
		Album b4 = new Album();
		b1.setLabel("Mystic Production");
		b1.setTitle("The Apostasy");
		b1.setYear(2007);
		
		b2.setLabel("Mystic Production");
		b2.setTitle("Evangelion");
		b2.setYear(2010);
		
		b3.setLabel("Mystic Production");
		b3.setTitle("Demigod");
		b3.setYear(2005);
		
		b4.setLabel("Nuclear Blast");
		b4.setTitle("Requiem for Indifferent");
		b4.setYear(2012);
		db.save(b1);
		db.save(b2);
		db.save(b3);
		db.save(b4);
		uow = new UnitOfWork();
		
	}

	@After
	public void tearDown() throws Exception {
		db.getAllItems().clear();
	}

	@Test
	public void testPersistAdd() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistDeleted() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistUpdated() {
		fail("Not yet implemented");
	}

	@Test
	public void test_sprawdza_poprawnosc_wyciagania_albumu_z_bazy() {
		IAlbumManager mgr = new MockAlbumManager(uow,db);
		Album album = mgr.get(1);
		Album album2 = mgr.get(1);
		assertNotNull("nie udało się wybrać książki z bazy"
				,album);
		assertTrue("problem z pobraniem wytworni"
				,album.getLabel().equals("Mystic Production"));
		assertEquals("problem z pobraniem wytworni",
				album.getLabel(),"Mystic Production");
		assertEquals("problem z tytułem",
				album.getTitle(),"The Apostasy");
		assertNotSame("obiekty wskazują na tą samą przestrzeń w pamieci"
				,album,album2);
	}

	@Test
	public void testGetAll() {
		fail("Not yet implemented");
	}

}