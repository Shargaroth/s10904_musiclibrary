package database.managers;

import java.util.List;

import database.IManager;
import model.Band;
import model.Album;

public interface IBandManager 
	extends IManager<Band>{
	
	public void setBandsOfAlbum(Album b);
}
