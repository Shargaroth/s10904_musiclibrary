package database.managers;

import model.Album;

import java.util.List;

import database.IManager;
public interface IAlbumManager extends IManager<Album>{

	public void setBands(Album b);
}
