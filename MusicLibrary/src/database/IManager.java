package database;

import java.util.List;

import model.EntityBase;

public interface IManager<T extends EntityBase> {

	public void save(T obj);
	public void delete(T obj);
	public void update(T obj);
	public T get(int id);
	public List<T> getAll(PageInfo request);
	public void saveChanges();
}
