package database;
import java.util.ArrayList;
import java.util.List;

import model.*;

public class MockDb {

	private  ArrayList<EntityBase> allItems = new ArrayList<EntityBase>();
	private  int size;
	
	public  ArrayList<EntityBase> getAllItems()
	{
		return allItems;
	}
	
	public  EntityBase get(int id)
	{
		return allItems.get(id);
	}
	
	public  void save(EntityBase item)
	{
		size++;
		item.setId(size);
		allItems.add(item);
	}
	
	public  void delete(EntityBase item)
	{
		allItems.remove(item);
	}
	
	public  <T extends EntityBase> List<T> getItemsByType(Class<T> c)
	{
		List<T> result = new ArrayList<T>();
		for(EntityBase item: allItems)
		{
			if(item.getClass().getName().equals(c.getName()))
			{
				result.add((T)item);
			}
		}
		
		return result;
	}
	
}
