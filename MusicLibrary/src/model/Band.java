package model;

public class Band extends EntityBase {
	
	private String name;
	private String genre;
	
	public String toString()
	{
		return "Name: "+this.name+"\n" +
				"Genre: " +this.genre;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	protected String showDetails() {
		// TODO Auto-generated method stub
		return this.toString();
	}
	
	
}
