package model;

import enums.MagazineType;

public class Magazine extends EntityBase{

	private int number;
	private MagazineType typeOfMagazine;
	
	@Override
	protected String showDetails() {
		String details;
		details="\nNumber of magazine: "+this.number+"\n";
		details+="Type: "+ typeOfMagazine;
		return details;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public MagazineType getTypeOfMagazine() {
		return typeOfMagazine;
	}

	public void setTypeOfMagazine(MagazineType typeOfMagazine) {
		this.typeOfMagazine = typeOfMagazine;
	}

	
}
