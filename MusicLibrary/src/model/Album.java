package model;

import java.util.*;

public class Album extends EntityBase {

	private ArrayList<Band> bands;
	private String label;
	
	@Override
	protected String showDetails() {
		
		String details;
		details="\nBands:";
		for(Band a : bands)
		{
			details+="\nb"+a;
		}
		details+="Label: "+this.label;
		return details;
	}

	public ArrayList<Band> getBands() {
		return bands;
	}

	public void setBands(ArrayList<Band> arrayList) {
		this.bands = arrayList;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	

}
