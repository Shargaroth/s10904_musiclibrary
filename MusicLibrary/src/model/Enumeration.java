package model;

public class Enumeration extends EntityBase {

	private int intKey;
	private String stringKey;
	private String value;
	private String enumName;
	
	public Enumeration()
	{}
	
	public Enumeration(int intKey, String stringKey, String value,
			String enumName) {
		super();
		this.intKey = intKey;
		this.stringKey = stringKey;
		this.value = value;
		this.enumName = enumName;
	}

	public int getIntKey() {
		return intKey;
	}

	public void setIntKey(int intKey) {
		this.intKey = intKey;
	}

	public String getStringKey() {
		return stringKey;
	}

	public void setStringKey(String stringKey) {
		this.stringKey = stringKey;
	}
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	public String getEnumName() {
		return enumName;
	}
	public void setEnumName(String enumName) {
		this.enumName = enumName;
	}

	@Override
	protected String showDetails() {
		// TODO Auto-generated method stub
		return toString();
	}

}
