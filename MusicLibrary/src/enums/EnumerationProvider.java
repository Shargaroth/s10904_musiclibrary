package enums;


public class EnumerationProvider {
	
	public static Object Provide(int intKey, String enumName)
	{
		EnumNames name = EnumNames.valueOf(enumName);
		switch(name)
		{
		case MagazineType:
		{
			return MagazineType.values()[intKey];
		}
		default:
			break;
		}
		return null;
	}
}
